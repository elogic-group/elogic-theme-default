/**
* Show/Hide Password
* Overridden to add eyes svg icons
* and add a possibility to show/hide specific password
*/

define([
    'jquery',
    'uiComponent'
], function ($, Component) {
    'use strict';

    return Component.extend({
        passwordSelector: '',
        passwordInputType: 'password',
        textInputType: 'text',

        defaults: {
            isPasswordVisible: false
        },

        /**
         * Init function
         */
        initObservable: function () {
            this._super()
                .observe(['isPasswordVisible']);

            var showPassIcon = '<span class="show-pass __show">\n' +
                    '<svg width="20" height="18" viewBox="0 0 20 14" class="icon icon-show-pass">\n' +
                        '<use xlink:href="#icon-eye"></use>\n' +
                    '</svg>\n' +
                    '<svg width="20" height="19" viewBox="0 0 20 19" class="icon icon-hide-pass">\n' +
                        '<use xlink:href="#icon-eye-crossed"></use>\n' +
                    '</svg>\n' +
                '</span>';

            $(this.passwordSelector).each(function () {
                $(this).wrap('<div class="password-wrapper"></div>');
                $(this).after(showPassIcon);
            });

            this._showPassword();
        },

        /**
         * Show/Hide password
         * @private
         */
        _showPassword: function () {
            $(this.passwordSelector).each(function () {
                var passInput = $(this);

                $(this).siblings('.show-pass').on('click', function () {
                    if (passInput.attr('type') === 'password') {
                        $(this).removeClass('__show').addClass('__hide');
                        passInput.attr('type', 'text');
                    } else {
                        $(this).removeClass('__hide').addClass('__show');
                        passInput.attr('type', 'password');
                    }
                });
            });
        }
    });
});
