<?php
/**
 * Elogic Default theme
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::THEME, 'frontend/Elogic/default', __DIR__);
