/**
 * Magento Theme - Messages
 * Overridden to add sticky functionality, hide messages after 10 seconds
 */

define([
    'ko',
    'jquery',
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'underscore',
    'escaper',
    'jquery/jquery-storageapi'
], function (ko, $, Component, customerData, _, escaper) {
    'use strict';

    return Component.extend({
        defaults: {
            cookieMessages: [],
            messages: [],
            allowedTags: ['div', 'span', 'b', 'strong', 'i', 'em', 'u', 'a'],
            isHeaderFixed: false,
            stickyHeaderSelector: '.has-sticky-header',
            fixedMessagesClass: '',
            headerSelector: '',
            style: ko.observable({})
        },

        /**
         * Extends Component object by storage observable messages.
         */
        initialize: function () {
            this._super();
            if (this.isHeaderFixed && this.headerSelector) {
                this.fixedMessagesClass = 'fixed ';
                this.calculatePosition();

                $(window).off().on('scroll resize', function () {
                    this.calculatePosition();
                }.bind(this));
            }

            this.cookieMessages = _.unique($.cookieStorage.get('mage-messages'), 'text');
            this.messages = customerData.get('messages').extend({
                disposableCustomerData: 'messages'
            });

            // Force to clean obsolete messages
            if (!_.isEmpty(this.messages().messages)) {
                customerData.set('messages', {});
            }

            $.mage.cookies.set('mage-messages', '', {
                samesite: 'strict',
                domain: ''
            });

            this.messages.subscribe(function (value) {
                //remove messages after 10 seconds
                if (!_.isEmpty(value)) {
                    setTimeout(function () {
                        customerData.set('messages', {});
                    }, 10000);
                }
            });
        },

        /**
         * Prepare the given message to be rendered as HTML
         *
         * @param {String} message
         * @return {String}
         */
        prepareMessageForHtml: function (message) {
            return escaper.escapeHtml(message, this.allowedTags);
        },

        /**
         * Hide messages by click on wrapper.
         */
        closeMessageWrapper: function () {
            this.messages = customerData.get('messages').extend({
                disposableCustomerData: 'messages'
            });

            if (!_.isEmpty(this.messages().messages)) {
                customerData.set('messages', {});
            } else {
                $('.cookie-messages').remove();
                $.cookieStorage.set('mage-messages', '');
            }
        },

        /**
         * Calculate position of element.
         */
        calculatePosition: function () {
            var headerHeight = $(this.headerSelector).outerHeight();

            if (headerHeight) {
                this.style({top: headerHeight + 1 + 'px'});
            }
        }
    });
});
